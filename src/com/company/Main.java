package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Stack s = new Stack();

        s.push("Ala");
        s.push(" ma");
        s.push(" kota.");

        System.out.println(s.get());

        System.out.println(s.pop());
        System.out.println(s.pop());
        System.out.println(s.pop());

        System.out.println(s.get());
    }
}
