package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Stack implements StackOperations{

    private List<String> stack;

    public Stack() {
       stack = new ArrayList<>();
    }

    @Override
    public List<String> get() {
        return stack;
    }

    @Override
    public Optional<String> pop() {

        if(stack.isEmpty()){
            return Optional.empty();
        }

        String head = stack.get(0);

        stack.remove(0);

        return Optional.of(head);

    }

    @Override
    public void push(String item) {
        stack.add(0, item);
    }
}
